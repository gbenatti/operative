
using System;
using NUnit.Framework;

using Operative.Core;
using Operative.Platform;

namespace Operative.Tests
{
	[TestFixture]
	public class GameEngineTest
	{
		[Test]
		public void GameEngineStartShouldCallPlatformStart()
		{
			var platform = new StubPlatform();
			var engine = new GameEngine(platform);

			Assert.IsFalse(platform.StartExecuted);

			engine.Start();

			Assert.IsTrue(platform.StartExecuted);
		}

		[Test]
		public void GameEngineShutdownShouldCallPlatformShutdown()
		{
			var platform = new StubPlatform();
			var engine = new GameEngine(platform);
			
			Assert.IsFalse(platform.ShutdownExecuted);

			engine.Start();
			
			Assert.IsFalse(platform.ShutdownExecuted);
		}

		[Test]
		public void GameEngineStartShouldAddHandlersToPlatformUpdateAndDraw()
		{
			var platform = new StubPlatform();
			Assert.AreEqual(0, platform.UpdateCount);
			Assert.AreEqual(0, platform.DrawCount);

			var engine = new GameEngine(platform);
			Assert.AreEqual(0, platform.UpdateCount);
			Assert.AreEqual(0, platform.DrawCount);

			engine.Start();
			Assert.AreEqual(1, platform.UpdateCount);
			Assert.AreEqual(1, platform.DrawCount);
		}

		[Test]
		public void GameEngineShutdownShouldRemoveHadlersToPlatformUpdateAndDraw()
		{
			var platform = new StubPlatform();
			var engine = new GameEngine(platform);

			engine.Start();
			Assert.AreEqual(1, platform.UpdateCount);
			Assert.AreEqual(1, platform.DrawCount);

			engine.Shutdown();
			Assert.AreEqual(0, platform.UpdateCount);
			Assert.AreEqual(0, platform.DrawCount);
		}

		class StubPlatform : IPlatform
		{
			#region IPlatform implementation

			public int UpdateCount
			{
				get;
				set;
			}

			public int DrawCount
			{
				get;
				set;
			}

			public event EventHandler<PlatformUpdateArgs> Update
			{
				add
				{
					UpdateCount++;
				}
				remove
				{
					UpdateCount--;
				}
			}

			public event EventHandler<PlatformDrawArgs> Draw
			{
				add
				{
					DrawCount++;
				}
				remove
				{
					DrawCount--;
				}
			}

			public bool StartExecuted
			{
				get;
				private set;
			}

			public bool ShutdownExecuted
			{
				get;
				private set;
			}

			public bool UpdateExecuted
			{
				get;
				private set;
			}

			public bool DrawExecuted
			{
				get;
				private set;
			}

			public void Start()
			{
				StartExecuted = true;
			}

			public void Shutdown()
			{
				ShutdownExecuted = true;
			}

			public void OnUpdate(PlatformUpdateArgs args)
			{
				UpdateExecuted = true;
			}

			public void OnDraw(PlatformDrawArgs args)
			{
				DrawExecuted = true;
			}

			#endregion


		}
	}
}

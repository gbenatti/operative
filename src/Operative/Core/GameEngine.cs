using System;

using Operative.Platform;
using OpenTK.Graphics.ES20;
using System.Drawing;

namespace Operative.Core
{
	public class GameEngine
	{
		IPlatform platform;

		public GameEngine(IPlatform platform)
		{
			this.platform = platform;
		}

		public void Start()
		{
			platform.Start();
			platform.Draw += Draw;
			platform.Update += Update;
		}

		public void Shutdown()
		{
			platform.Draw -= Draw;
			platform.Update -= Update;
			platform.Shutdown();
		}

		private void Draw(object sender, PlatformDrawArgs args)
		{
			GL.ClearColor(Color.Blue);
			GL.Clear(ClearBufferMask.ColorBufferBit);
		}

		private void Update(object sender, PlatformUpdateArgs args)
		{
		}
	}
}


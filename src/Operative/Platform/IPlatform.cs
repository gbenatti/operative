using System;

namespace Operative.Platform
{
	public interface IPlatform
	{
		event EventHandler<PlatformUpdateArgs> Update;
		event EventHandler<PlatformDrawArgs> Draw;

		void Start();
		void Shutdown();

		void OnUpdate(PlatformUpdateArgs args);
		void OnDraw(PlatformDrawArgs args);
	}
}


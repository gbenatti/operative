using System;
using MonoTouch.UIKit;

namespace Operative.Platform
{
	public class iOSPlatformWindow : IPlatform
	{
		#region IPlatform implementation

		public event EventHandler<PlatformUpdateArgs> Update;

		public event EventHandler<PlatformDrawArgs> Draw;

		public void OnUpdate(PlatformUpdateArgs args)
		{
			if (Update != null)
				Update(this, args);
		}

		public void OnDraw(PlatformDrawArgs args)
		{
			if (Draw != null)
				Draw(this, args);
		}

		public void Start()
		{
			UIApplication.SharedApplication.SetStatusBarHidden(true, UIStatusBarAnimation.Fade);
			
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			ViewController = new iOSPlatformViewController(this);
			Window.RootViewController = ViewController;
			Window.MakeKeyAndVisible();
		}
		
		public void Shutdown()
		{
		}

		#endregion

		public UIWindow Window
		{
			get;
			private set;
		}

		public UIViewController ViewController
		{
			get;
			private set;
		}

		public iOSPlatformWindow()
		{
		}
	}
}

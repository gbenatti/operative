using System;

using MonoTouch.UIKit;
using MonoTouch.GLKit;
using MonoTouch.OpenGLES;
using MonoTouch.Foundation;

namespace Operative.Platform
{
	public class iOSPlatformViewController : GLKViewController
	{
		EAGLContext context;
		GLKView glkView;

		public IPlatform Platform
		{
			get;
			private set;
		}
				
		public iOSPlatformViewController(IPlatform platform)
		{
			Platform = platform;
		}

		public override void ViewDidLoad()
		{
			context = new EAGLContext(EAGLRenderingAPI.OpenGLES2);
					
			glkView = (GLKView)View;
			glkView.Context = context;
			glkView.MultipleTouchEnabled = true;
			glkView.DrawInRect += Draw;
					
			View = glkView;
					
			PreferredFramesPerSecond = 60;
			var size = UIScreen.MainScreen.Bounds;
			View.ContentScaleFactor = UIScreen.MainScreen.Scale;
					
			EAGLContext.SetCurrentContext(context);
		}
				
		void Draw(object sender, GLKViewDrawEventArgs e)
		{
			Platform.OnDraw(new PlatformDrawArgs());
		}
				
		public override void Update()
		{
			Platform.OnUpdate(new PlatformUpdateArgs());
		}
				
		void ProcessTouches(NSSet touches)
		{
			foreach (UITouch touch in touches.ToArray<UITouch> ())
			{
				touch.LocationInView(touch.View);
			}
		}
				
		public override void TouchesBegan(NSSet touches, UIEvent evt)
		{
			ProcessTouches(touches);
		}
				
		public override void TouchesMoved(NSSet touches, UIEvent evt)
		{
			ProcessTouches(touches);
		}
				
	}

}
